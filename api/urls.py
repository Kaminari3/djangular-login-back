from django.urls import path
from django.conf.urls import url, include
from . import views
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token


router = routers.DefaultRouter()




urlpatterns = [
    
    # url(r'^',views.index, name='index'),
    #url(r'^login/$',views.UserView.as_view()),
    url(r'^register/$',views.RegisterUserView.as_view()),
    url(r'^login/', obtain_jwt_token),
    url(r'^auth-jwt-refresh/', refresh_jwt_token),
    url(r'^auth-jwt-verify/', verify_jwt_token),
    
]

