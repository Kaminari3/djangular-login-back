from django.shortcuts import render
from rest_framework import generics, permissions, status,viewsets
from api.models import User
from api.serializers import *
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework.permissions import AllowAny
# Create your views here.

class RegisterUserView(generics.CreateAPIView):
    
    permission_classes = (AllowAny,)    
    serializer_class = UserSerializer
    queryset = User.objects.all()  
       
    # def post (self,request,*args,**kwargs):
        
    #     serializer_context = {
    #         'request': request,
    #     }
    #     serializer = UserSerializer(data=request.data, context=serializer_context)
    #     if not serializer.is_valid():
    #         return Response({
	# 			"status":"failure",
	# 			"message": serializer.errors,
	# 			"errors": "erreur",
    #             "data":serializer.data
	# 			}, status= status.HTTP_400_BAD_REQUEST)
   
    #     serializer.save()
    #     return Response({
    #         "data":serializer.data,        
    #         "status":"success",
	# 		"message": "user successfully added",			
    #     }, status=status.HTTP_201_CREATED)