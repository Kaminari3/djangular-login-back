# from django.db import models

# # Create your models here.
# class User (models.Model):
#     email = models.CharField(max_length=30, blank=False, null=False)
#     password = models.CharField(max_length=30, blank=False, null=False)
#     first_name = models.CharField(max_length=30, blank=False, null=False)
#     last_name = models.CharField(max_length=30, blank=False, null=False)
#     address = models.CharField(max_length=50 ,blank=True, null=True)

#     def __str__(self):
#     	return str(self.email) +' '+str(self.password)+' '+str(self.first_name)+' '+str(self.last_name)+' '+str(self.address)

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

class User(AbstractUser):
    username = models.CharField(blank=True, null=True, max_length=50)
    email = models.EmailField(_('email'), unique=True)
    

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        return "{}".format(self.email)
    
class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    title = models.CharField(max_length=15)
    dob = models.DateField()
    address = models.CharField(max_length=255)
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    zip = models.CharField(max_length=5)